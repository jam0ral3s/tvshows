# TvShows

Test application

## Getting Started

To be compiled it is necessary to add the API key of the "The Movie Database”:
Get API key from: https://developers.themoviedb.org/3/getting-started/introduction

Put it in the local.properties file

```
themoviedb_apiKey=“XXXXXX"
```

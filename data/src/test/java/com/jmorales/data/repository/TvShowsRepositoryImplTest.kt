package com.jmorales.data.repository

import com.jmorales.data.model.*
import com.jmorales.data.services.TvService
import com.jmorales.domain.model.PaginatedResultModel
import com.jmorales.domain.model.TvShowModel
import io.reactivex.Observable
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyMap
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class TvShowsRepositoryImplTest {

    @Mock
    private val tvService: TvService? = null

    @Mock
    private val tvShowDtoMapper: TvShowDtoMapper? = null

    @Mock
    private val tvShowDetailDtoMapper: TvShowDetailDtoMapper? = null

    @Mock
    private val tvShowCreditDtoMapper: TvShowCreditDtoMapper? = null

    @InjectMocks
    private val tvShowsRepositoryImpl: TvShowsRepositoryImpl? = null


    @Test
    fun checkGetPopularTvShowsSuccess() {
        // Mock
        val mockitoResult = Observable.just(getPaginatedResultDto())
        Mockito.`when`(tvService!!.getPopularShows(anyMap())).thenReturn(mockitoResult)

        // Trigger
        val testObserver = tvShowsRepositoryImpl!!.getPopularTvShows(1).test()

        // Prepare Result
        val paginatedResultModel = getPaginatedResultModel()

        // Validation
        testObserver.assertComplete()
        testObserver.assertValueCount(1)
        testObserver.assertNoErrors()
        testObserver.assertValues(paginatedResultModel)

        // clean up
        testObserver.dispose()
    }

    @Test
    fun checkGetPopularTvShowsRetrofitError() {
        // Mock
        Mockito.`when`(tvService!!.getPopularShows(anyMap())).thenReturn(Observable.error(Exception()))

        // Trigger
        val testObserver = tvShowsRepositoryImpl!!.getPopularTvShows(1).test()

        // Validation
        testObserver.assertValueCount(0)
        testObserver.assertError(Exception::class.java)

        // clean up
        testObserver.dispose()
    }

    @Test
    fun checkGetPopularTvShowsError() {
        // Mock
        val mockitoResult = Observable.just(getErrorResponse())
        Mockito.`when`(tvService!!.getPopularShows(anyMap())).thenReturn(mockitoResult)

        // Trigger
        val testObserver = tvShowsRepositoryImpl!!.getPopularTvShows(1).test()

        // Validation
        testObserver.assertValueCount(0)
        testObserver.assertError(Exception::class.java)

        // clean up
        testObserver.dispose()
    }

    private fun getPaginatedResultModel(): PaginatedResultModel<TvShowModel> {
        val tvShowListModel =
            mutableListOf(
                TvShowModel(
                    id = 1,
                    name = "Test",
                    originalName = "Test",
                    popularity = 1,
                    posterPath = "",
                    backdropPath = "",
                    voteAverage = 1,
                    overview = "",
                    firstAirDate = "",
                    originCountry = emptyList(),
                    genreIds = emptyList(),
                    originalLanguage = "",
                    voteCount = 1
                )
            )

        return PaginatedResultModel(
            page = 1,
            results = tvShowListModel,
            totalResult = 1,
            totalPages = 1
        )
    }

    private fun getPaginatedResultDto(): PaginatedResultDto<TvShowDto> {
        val tvShowListDto =
            mutableListOf(
                TvShowDto(
                    id = 1,
                    name = "Test",
                    originalName = "Test",
                    popularity = 1,
                    posterPath = "",
                    backdropPath = "",
                    voteAverage = 1,
                    overview = "",
                    firstAirDate = "",
                    originCountry = emptyList(),
                    genreIds = emptyList(),
                    originalLanguage = "",
                    voteCount = 1
                )
            )

        return PaginatedResultDto(
            page = 1,
            results = tvShowListDto,
            totalResults = 1,
            totalPages = 1,
            statusMessage = "",
            statusCode = 200
        )
    }

    private fun getErrorResponse(): PaginatedResultDto<TvShowDto> {
        return PaginatedResultDto(
            page = 0,
            results = emptyList(),
            totalResults = 0,
            totalPages = 0,
            statusMessage = "Invalid API key: You must be granted a valid key.",
            statusCode = 7
        )
    }

}
package com.jmorales.data.services

import com.jmorales.data.model.PaginatedResultDto
import com.jmorales.data.model.TvShowCreditsDto
import com.jmorales.data.model.TvShowDetailDto
import com.jmorales.data.model.TvShowDto
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface TvService {

    @GET("/3/tv/popular")
    fun getPopularShows(@QueryMap(encoded = true) options: Map<String, String>): Observable<PaginatedResultDto<TvShowDto>>

    @GET("/3/tv/{id}")
    fun getDetailTvShow(@Path("id") id: Int): Observable<TvShowDetailDto>

    @GET("/3/tv/{id}/credits")
    fun geTvShowCredits(@Path("id") id: Int): Observable<TvShowCreditsDto>

    @GET("/3/tv/{id}/similar")
    fun geTvShowRelated(@Path("id") id: Int, @QueryMap(encoded = true) options: Map<String, String>): Observable<PaginatedResultDto<TvShowDto>>
}
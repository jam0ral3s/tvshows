package com.jmorales.data.repository

import com.jmorales.data.model.TvShowCreditDtoMapper
import com.jmorales.data.model.TvShowDetailDtoMapper
import com.jmorales.data.model.TvShowDtoMapper
import com.jmorales.data.services.TvService
import com.jmorales.domain.model.PaginatedResultModel
import com.jmorales.domain.model.TvShowCreditModel
import com.jmorales.domain.model.TvShowDetailModel
import com.jmorales.domain.model.TvShowModel
import com.jmorales.domain.repository.TvShowsRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import javax.inject.Inject

class TvShowsRepositoryImpl @Inject constructor(
    private val api: TvService,
    private val mapperTvShowMapper: TvShowDtoMapper,
    private val mapperTvShowDetailMapper: TvShowDetailDtoMapper,
    private val mapperCreditMapper: TvShowCreditDtoMapper
) :
    TvShowsRepository {

    override fun getPopularTvShows(page: Int): Observable<PaginatedResultModel<TvShowModel>> {
        val queryParams = HashMap<String, String>()
        queryParams["page"] = page.toString()


        return api.getPopularShows(queryParams)
            .doOnError {
                //todo: check error
                throw when (it) {
                    is HttpException -> it
                    else -> it
                }
            }
            .flatMap {
                if (it.statusCode == 200) {
                    val tvShowModels = mapperTvShowMapper.mapToDomain(it.results)
                    Observable.just(PaginatedResultModel(it.page, tvShowModels, it.totalResults, it.totalPages))
                } else {
                    //todo: check error
                    throw Exception()
                }
            }

    }

    override fun getTvShowDetail(id: Int): Observable<TvShowDetailModel> {
        return api.getDetailTvShow(id)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                throw when (it) {
                    is HttpException -> it
                    else -> it
                }
            }
            .flatMap {
                if (it.statusCode == 200) {
                    val tvShowDetailModel = mapperTvShowDetailMapper.mapToDomain(it)
                    Observable.just(tvShowDetailModel)
                } else {
                    //todo: check error
                    throw Exception()
                }
            }
    }

    override fun getTvShowCredits(id: Int): Observable<List<TvShowCreditModel>> {
        return api.geTvShowCredits(id)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                throw when (it) {
                    is HttpException -> it
                    else -> it
                }
            }
            .flatMap {
                if (it.statusCode == 200) {
                    val tvShowDetailModel = mapperCreditMapper.mapToDomain(it.cast)
                    Observable.just(tvShowDetailModel)
                } else {
                    //todo: check error
                    throw Exception()
                }
            }
    }

    override fun getTvShowRelated(id: Int, page: Int): Observable<PaginatedResultModel<TvShowModel>> {
        val queryParams = HashMap<String, String>()
        queryParams["page"] = page.toString()


        return api.geTvShowRelated(id, queryParams)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                //todo: check error
                throw when (it) {
                    is HttpException -> it
                    else -> it
                }
            }
            .flatMap {
                if (it.statusCode == 200) {
                    val tvShowModels = mapperTvShowMapper.mapToDomain(it.results)
                    Observable.just(PaginatedResultModel(it.page, tvShowModels, it.totalResults, it.totalPages))
                } else {
                    //todo: check error
                    throw Exception()
                }
            }
    }
}
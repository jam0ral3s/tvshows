package com.jmorales.data.model

import com.google.gson.annotations.SerializedName
import com.jmorales.domain.model.TvShowModel
import javax.inject.Inject

data class TvShowDto(
    val id: Int,
    val name: String,
    @SerializedName("original_name")
    val originalName: String,
    val popularity: Number,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("backdrop_path")
    val backdropPath: String?,
    @SerializedName("vote_average")
    val voteAverage: Number,
    val overview: String,
    @SerializedName("first_air_date")
    val firstAirDate: String,
    @SerializedName("origin_country")
    val originCountry: List<String>,
    @SerializedName("genre_ids")
    val genreIds: List<Int>,
    @SerializedName("original_language")
    val originalLanguage: String,
    @SerializedName("vote_count")
    val voteCount: Int
)

open class TvShowDtoMapper @Inject constructor() {
    fun mapToDomain(dto: TvShowDto): TvShowModel = TvShowModel(
        id = dto.id,
        name = dto.name,
        originalName = dto.originalName,
        popularity = dto.popularity,
        posterPath = dto.posterPath,
        backdropPath = dto.backdropPath,
        voteAverage = dto.voteAverage,
        overview = dto.overview,
        firstAirDate = dto.firstAirDate,
        originCountry = dto.originCountry,
        genreIds = dto.genreIds,
        originalLanguage = dto.originalLanguage,
        voteCount = dto.voteCount
    )

    fun mapToDomain(items: List<TvShowDto>): List<TvShowModel> {
        return items.map {
            mapToDomain(it)
        }
    }
}
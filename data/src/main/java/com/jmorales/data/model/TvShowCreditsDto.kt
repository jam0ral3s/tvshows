package com.jmorales.data.model

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName
import java.lang.reflect.Type

data class TvShowCreditsDto(
    val id: Int,
    val cast: List<TvShowCreditDto>,
    val statusMessage: String?,
    @SerializedName("status_code")
    var statusCode: Int?
)

class DeserializerTvShowCreditsDto : JsonDeserializer<TvShowCreditsDto> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): TvShowCreditsDto? {
        checkNotNull(json)
        checkNotNull(typeOfT)
        checkNotNull(context)

        val jsonObject = json.asJsonObject

        var statusCode = 200
        if (jsonObject.has("status_code")) {
            statusCode = jsonObject.get("status_code").asInt
        }

        val tvShowCreditsDtoDto: TvShowCreditsDto = Gson().fromJson(json, typeOfT)
        tvShowCreditsDtoDto.statusCode = statusCode

        return tvShowCreditsDtoDto
    }
}
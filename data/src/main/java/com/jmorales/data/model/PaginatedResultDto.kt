package com.jmorales.data.model

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName
import java.lang.reflect.Type


data class PaginatedResultDto<T>(
    val page: Int,
    val results: List<T>,
    @SerializedName("total_results")
    val totalResults: Int,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("status_message")
    val statusMessage: String?,
    @SerializedName("status_code")
    val statusCode: Int?
)

class DeserializerPaginatedResultDto<T> : JsonDeserializer<PaginatedResultDto<T>> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): PaginatedResultDto<T> {
        checkNotNull(json)
        checkNotNull(typeOfT)
        checkNotNull(context)

        val jsonObject = json.asJsonObject

        var statusCode = 200
        if (jsonObject.has("status_code")) {
            statusCode = jsonObject.get("status_code").asInt
        }

        val tmpPaginatedResultDto: PaginatedResultDto<T> = Gson().fromJson(json, typeOfT)

        return PaginatedResultDto(
            tmpPaginatedResultDto.page,
            tmpPaginatedResultDto.results,
            tmpPaginatedResultDto.totalResults,
            tmpPaginatedResultDto.totalPages,
            tmpPaginatedResultDto.statusMessage ?: "",
            statusCode
        )
    }
}
package com.jmorales.data.model

data class GenreDto(
    val id: Int,
    val name: String
)

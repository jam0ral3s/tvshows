package com.jmorales.data.model

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName
import com.jmorales.domain.model.GenreModel
import com.jmorales.domain.model.TvShowDetailModel
import java.lang.reflect.Type
import javax.inject.Inject

data class TvShowDetailDto(
    val id: Int,
    val name: String,
    @SerializedName("original_name")
    val originalName: String,
    val popularity: Number,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("backdrop_path")
    val backdropPath: String?,
    @SerializedName("vote_average")
    val voteAverage: Number,
    val overview: String,
    @SerializedName("first_air_date")
    val firstAirDate: String,
    @SerializedName("origin_country")
    val originCountry: List<String>,
    val genres: List<GenreDto>,
    @SerializedName("original_language")
    val originalLanguage: String,
    @SerializedName("vote_count")
    val voteCount: Int,
    @SerializedName("number_of_seasons")
    val numberSeasons: Int,
    @SerializedName("status_message")
    val statusMessage: String?,
    @SerializedName("status_code")
    var statusCode: Int?
)


open class TvShowDetailDtoMapper @Inject constructor() {
    fun mapToDomain(dto: TvShowDetailDto): TvShowDetailModel = TvShowDetailModel(
        id = dto.id,
        name = dto.name,
        originalName = dto.originalName,
        popularity = dto.popularity,
        posterPath = dto.posterPath,
        backdropPath = dto.backdropPath,
        voteAverage = dto.voteAverage,
        overview = dto.overview,
        firstAirDate = dto.firstAirDate,
        originCountry = dto.originCountry,
        genres = dto.genres.map { GenreModel(it.id, it.name) },
        originalLanguage = dto.originalLanguage,
        voteCount = dto.voteCount,
        numberSeasons = dto.numberSeasons

    )
}

class DeserializerTvShowDetailDto : JsonDeserializer<TvShowDetailDto> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): TvShowDetailDto? {
        checkNotNull(json)
        checkNotNull(typeOfT)
        checkNotNull(context)

        val jsonObject = json.asJsonObject

        var statusCode = 200
        if (jsonObject.has("status_code")) {
            statusCode = jsonObject.get("status_code").asInt
        }

        val tvShowsDetailDto: TvShowDetailDto = Gson().fromJson(json, typeOfT)
        tvShowsDetailDto.statusCode = statusCode

        return tvShowsDetailDto;
    }
}
package com.jmorales.data.model

import com.google.gson.annotations.SerializedName
import com.jmorales.domain.model.TvShowCreditModel
import javax.inject.Inject

data class TvShowCreditDto(
    val id: Int,
    val name: String,
    val character: String,
    @SerializedName("profile_path")
    val profilePath: String?,
    val order: Int
)


open class TvShowCreditDtoMapper @Inject constructor() {
    fun mapToDomain(dto: TvShowCreditDto): TvShowCreditModel = TvShowCreditModel(
        id = dto.id,
        name = dto.name,
        character = dto.character,
        order = dto.order,
        profileImage = dto.profilePath
    )

    fun mapToDomain(items: List<TvShowCreditDto>): List<TvShowCreditModel> {
        return items.map {
            mapToDomain(it)
        }
    }
}
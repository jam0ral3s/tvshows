package com.jmorales.domain.usecase

import com.jmorales.domain.model.PaginatedResultModel
import com.jmorales.domain.model.TvShowModel
import com.jmorales.domain.repository.TvShowsRepository
import io.reactivex.Observable
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class GetPopularTvShowsUseCaseTest {

    @InjectMocks
    private val getPopularTvShowsUseCase: GetPopularTvShowsUseCase? = null

    @Mock
    private val tvShowsRepository: TvShowsRepository? = null

    @Test
    fun checkGetPopularTvShowsSuccess() {
        val paginatedResultModel = getPaginatedResultModel()

        val observable = Observable.just(paginatedResultModel)
        Mockito.`when`(tvShowsRepository!!.getPopularTvShows(anyInt())).thenReturn(observable)

        // Trigger
        val testObserver = getPopularTvShowsUseCase!!.execute(1).test()

        // Validation
        testObserver.assertComplete()
        testObserver.assertValueCount(2)
        testObserver.assertNoErrors()
        testObserver.assertValues(
            GetPopularTvShowsUseCase.CustomResult.Loading,
            GetPopularTvShowsUseCase.CustomResult.Success(paginatedResultModel)
        )

        // clean up
        testObserver.dispose()
    }

    @Test
    fun checkGetPopularTvShowsException() {
        val exeption: Exception = Exception("")
        Mockito.`when`(tvShowsRepository!!.getPopularTvShows(anyInt())).thenReturn(Observable.error(exeption))

        // Trigger
        val testObserver = getPopularTvShowsUseCase!!.execute(1).test()

        // Validation
        testObserver.assertValueCount(2)
        testObserver.assertValues(
            GetPopularTvShowsUseCase.CustomResult.Loading,
            GetPopularTvShowsUseCase.CustomResult.Failure(exeption)
        )

        // clean up
        testObserver.dispose() // Pro-tip: don't forget this
    }

    private fun getPaginatedResultModel(): PaginatedResultModel<TvShowModel> {
        val tvShowListModel =
            mutableListOf(
                TvShowModel(
                    id = 1,
                    name = "Test",
                    originalName = "Test",
                    popularity = 1,
                    posterPath = "",
                    backdropPath = "",
                    voteAverage = 1,
                    overview = "",
                    firstAirDate = "",
                    originCountry = emptyList(),
                    genreIds = emptyList(),
                    originalLanguage = "",
                    voteCount = 1
                )
            )

        return PaginatedResultModel(
            page = 1,
            results = tvShowListModel,
            totalResult = 1,
            totalPages = 1
        )
    }
}
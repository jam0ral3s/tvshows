package com.jmorales.domain.model

import javax.inject.Inject

data class TvShowModel(
    val id: Int,
    val name: String,
    val originalName: String,
    val popularity: Number,
    val posterPath: String?,
    val backdropPath: String?,
    val voteAverage: Number,
    val overview: String,
    val firstAirDate: String,
    val originCountry: List<String>,
    val genreIds: List<Int>,
    val originalLanguage: String,
    val voteCount: Int
)
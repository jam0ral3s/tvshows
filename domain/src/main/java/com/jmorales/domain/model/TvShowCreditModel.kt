package com.jmorales.domain.model


data class TvShowCreditModel(
    val id: Int,
    val name: String,
    val character: String,
    val profileImage: String?,
    val order: Int
)
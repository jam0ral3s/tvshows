package com.jmorales.domain.model

data class PaginatedResultModel<T>(
    val page: Int,
    val results: List<T>,
    val totalResult: Int,
    val totalPages: Int
)
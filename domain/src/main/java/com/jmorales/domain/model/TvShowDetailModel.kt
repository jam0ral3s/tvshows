package com.jmorales.domain.model


data class TvShowDetailModel(
    val id: Int,
    val name: String,
    val originalName: String,
    val popularity: Number,
    val posterPath: String?,
    val backdropPath: String?,
    val voteAverage: Number,
    val overview: String,
    val firstAirDate: String,
    val originCountry: List<String>,
    val genres: List<GenreModel>,
    val numberSeasons: Int,
    val originalLanguage: String,
    val voteCount: Int
)
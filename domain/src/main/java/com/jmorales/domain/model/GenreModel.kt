package com.jmorales.domain.model


data class GenreModel(
    val id: Int,
    val name: String
)
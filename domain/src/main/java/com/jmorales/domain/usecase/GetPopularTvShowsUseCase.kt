package com.jmorales.domain.usecase

import com.jmorales.domain.model.PaginatedResultModel
import com.jmorales.domain.model.TvShowModel
import com.jmorales.domain.repository.TvShowsRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetPopularTvShowsUseCase @Inject constructor(private val repository: TvShowsRepository) {

    sealed class CustomResult {
        object Loading : CustomResult()
        data class Success(val paginatedTvShows: PaginatedResultModel<TvShowModel>) : CustomResult()
        data class Failure(val throwable: Throwable) : CustomResult()
    }

    fun execute(page: Int): Observable<CustomResult> {
        return repository.getPopularTvShows(page)
            .map { CustomResult.Success(it) as CustomResult }
            .onErrorReturn { CustomResult.Failure(it) }
            .startWith(CustomResult.Loading as CustomResult)

    }
}

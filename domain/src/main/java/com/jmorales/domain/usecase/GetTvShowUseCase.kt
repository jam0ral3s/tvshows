package com.jmorales.domain.usecase

import com.jmorales.domain.model.TvShowDetailModel
import com.jmorales.domain.repository.TvShowsRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetTvShowUseCase @Inject constructor(private val repository: TvShowsRepository) {

    sealed class CustomResult {
        object Loading : CustomResult()
        data class Success(val tvShowDetail: TvShowDetailModel) : CustomResult()
        data class Failure(val throwable: Throwable) : CustomResult()
    }

    fun execute(tvShowId: Int): Observable<CustomResult> {
        return repository.getTvShowDetail(tvShowId)
            .map { CustomResult.Success(it) as CustomResult }
            .onErrorReturn { CustomResult.Failure(it) }
            .startWith(CustomResult.Loading as CustomResult)

    }
}

package com.jmorales.domain.usecase

import com.jmorales.domain.model.TvShowCreditModel
import com.jmorales.domain.repository.TvShowsRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetTvShowCreditsUseCase @Inject constructor(private val repository: TvShowsRepository) {

    sealed class CustomResult {
        object Loading : CustomResult()
        data class Success(val tvShowCredits: List<TvShowCreditModel>) : CustomResult()
        data class Failure(val throwable: Throwable) : CustomResult()
    }

    fun execute(tvShowId: Int): Observable<CustomResult> {
        return repository.getTvShowCredits(tvShowId)
            .map { CustomResult.Success(it.sortedBy { it.order }) as CustomResult }
            .onErrorReturn { CustomResult.Failure(it) }
            .startWith(CustomResult.Loading as CustomResult)

    }
}

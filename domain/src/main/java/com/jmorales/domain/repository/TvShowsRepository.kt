package com.jmorales.domain.repository

import com.jmorales.domain.model.PaginatedResultModel
import com.jmorales.domain.model.TvShowCreditModel
import com.jmorales.domain.model.TvShowDetailModel
import com.jmorales.domain.model.TvShowModel
import io.reactivex.Observable
import io.reactivex.Single

interface TvShowsRepository {
    fun getPopularTvShows(page: Int = 1): Observable<PaginatedResultModel<TvShowModel>>
    fun getTvShowDetail(id: Int): Observable<TvShowDetailModel>
    fun getTvShowCredits(id: Int): Observable<List<TvShowCreditModel>>
    fun getTvShowRelated(id:Int, page: Int = 1): Observable<PaginatedResultModel<TvShowModel>>
}
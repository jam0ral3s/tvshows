package com.jmorales.aba.model

import com.jmorales.domain.model.TvShowCreditModel
import javax.inject.Inject

data class TvShowCreditItem(
    val id: Int,
    val character: String,
    val name: String,
    val profileImage: String?,
    val order: Int
)


class TvShowCreditItemMapper @Inject constructor() {
    fun mapToPresentation(tvShow: TvShowCreditModel): TvShowCreditItem {
        return TvShowCreditItem(
            id = tvShow.id,
            name = tvShow.name,
            character = tvShow.character,
            profileImage = tvShow.profileImage,
            order = tvShow.order
        )
    }

    fun mapToPresentation(tvShows: List<TvShowCreditModel>): List<TvShowCreditItem> {
        return tvShows.map { mapToPresentation(it) }
    }
}
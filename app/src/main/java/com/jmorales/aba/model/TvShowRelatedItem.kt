package com.jmorales.aba.model

import android.os.Parcelable
import com.jmorales.domain.model.TvShowModel
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class TvShowRelatedItem(
    val id: Int,
    val name: String,
    val posterPath: String?,
    val overview: String
) : Parcelable


class TvShowRelatedItemMapper @Inject constructor() {
    fun mapToPresentation(tvShow: TvShowModel): TvShowRelatedItem {
        return TvShowRelatedItem(
            id = tvShow.id,
            name = tvShow.name,
            posterPath = tvShow.posterPath,
            overview = tvShow.overview
        )
    }

    fun mapToPresentation(tvShows: List<TvShowModel>): List<TvShowRelatedItem> {
        return tvShows.map { mapToPresentation(it) }
    }
}
package com.jmorales.aba.model

import android.os.Parcelable
import com.jmorales.domain.model.TvShowModel
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class TvShowItem(
    val id: Int,
    val name: String,
    val posterPath: String?,
    val voteAverage: Number,
    val overview: String,
    val voteCount: Int
) : Parcelable {
    companion object {
        const val EXTRA_TVSHOWITEM = "extra_tv_show_item"
    }

}


class TvShowItemMapper @Inject constructor() {
    fun mapToPresentation(tvShow: TvShowModel): TvShowItem {
        return TvShowItem(
            id = tvShow.id,
            name = tvShow.name,
            posterPath = tvShow.posterPath,
            voteAverage = tvShow.voteAverage,
            overview = tvShow.overview,
            voteCount = tvShow.voteCount
        )
    }

    fun mapToPresentation(tvShows: List<TvShowModel>): List<TvShowItem> {
        return tvShows.map { mapToPresentation(it) }
    }
}
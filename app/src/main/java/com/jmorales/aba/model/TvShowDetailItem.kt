package com.jmorales.aba.model

import com.jmorales.domain.model.TvShowDetailModel
import javax.inject.Inject

data class TvShowDetailItem(
    val id: Int,
    val name: String,
    val popularity: Number,
    val posterPath: String?,
    val backdropPath: String?,
    val voteAverage: Number,
    val overview: String,
    val firstAirDate: String,
    val voteCount: Int,
    val genres: List<String>,
    val numberSeasons: Int
)


class TvShowDetailItemMapper @Inject constructor() {
    fun mapToPresentation(tvShow: TvShowDetailModel): TvShowDetailItem {
        return TvShowDetailItem(
            id = tvShow.id,
            name = tvShow.name,
            popularity = tvShow.popularity,
            posterPath = tvShow.posterPath,
            backdropPath = tvShow.backdropPath,
            voteAverage = tvShow.voteAverage,
            overview = tvShow.overview,
            firstAirDate = tvShow.firstAirDate,
            voteCount = tvShow.voteCount,
            genres = tvShow.genres.map { it.name },
            numberSeasons = tvShow.numberSeasons
        )
    }
}
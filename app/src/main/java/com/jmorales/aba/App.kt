package com.jmorales.aba

import android.app.Application
import android.content.Context
import com.jmorales.aba.base.di.ApiModule
import com.jmorales.aba.base.di.DaggerInjector
import com.jmorales.aba.base.di.Injector
import timber.log.Timber

class App : Application() {

    lateinit var injector: Injector private set

    companion object {
        lateinit var INSTANCE: App

        fun getAppContext(): App = INSTANCE

        fun get(context: Context): App {
            return context.applicationContext as App
        }
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this

        initDagger()
        initTimber()
    }

    private fun initDagger() {
        injector = DaggerInjector.builder()
            .apiModule(ApiModule())
            .build()
    }

    // Used for mock ApiModule on Testing
    fun setDaggerInjector(injector: Injector) {
        this.injector = injector
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
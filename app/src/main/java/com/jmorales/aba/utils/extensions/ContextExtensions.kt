package com.jmorales.aba.utils.extensions

import android.app.Activity
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.jmorales.aba.App
import com.jmorales.aba.base.di.Injector

val Activity.app: App get() = application as App
val Fragment.app: App get() = activity?.application as App

fun AppCompatActivity.getAppInjector(): Injector = (app).injector
fun Fragment.getAppInjector(): Injector = (app).injector







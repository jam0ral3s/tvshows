package com.jmorales.aba.utils

enum class Status {
    SUCCESS,
    FAILED
}

@Suppress("DataClassPrivateConstructor")
data class PaginationStatus private constructor(
    val status: Status,
    val message: String? = null
) {
    companion object {
        val LOADED = PaginationStatus(Status.SUCCESS)
        fun error(msg: String?) = PaginationStatus(Status.FAILED, msg)
    }
}
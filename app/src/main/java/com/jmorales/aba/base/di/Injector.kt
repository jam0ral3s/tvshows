package com.jmorales.aba.base.di

import com.jmorales.aba.ui.showDetail.detail.TvShowDetailFragment
import com.jmorales.aba.ui.showDetail.related.TvShowRelatedFragment
import com.jmorales.aba.ui.showsList.TvShowsListActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class, ViewModelModule::class, RepositoryModule::class])
interface Injector {
    fun inject(activity: TvShowsListActivity)
    fun inject(fragment: TvShowDetailFragment)
    fun inject(fragment: TvShowRelatedFragment)
}

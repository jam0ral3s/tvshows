package com.jmorales.aba.base.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.jmorales.aba.BuildConfig
import com.jmorales.data.model.*
import com.jmorales.data.services.TvService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
open class ApiModule {

    companion object {
        private const val BASE_URL = "https://api.themoviedb.org/"
        private const val CONNECT_TIMEOUT = 10L
        private const val READ_TIMEOUT = 10L
        private const val WRITE_TIMEOUT = 10L
    }

    @Provides
    @Singleton
    fun provideHttpClient(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger.DEFAULT)
        val clientBuilder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            clientBuilder.addInterceptor(httpLoggingInterceptor)
        }

        clientBuilder.addInterceptor {
            val originalRequest = it.request()
            val originalHttpUrl = originalRequest.url()

            // Added API_KEY of themoviedb in all request. Put themoviedb_apiKey="...." in your gradle.properties
            val url = originalHttpUrl.newBuilder()
                .addQueryParameter("api_key", BuildConfig.TheMovieDbApiKey)
                .build()

            val request = originalRequest.newBuilder().url(url).build()

            return@addInterceptor it.proceed(request)
        }

        clientBuilder.cache(null)
        clientBuilder.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
        clientBuilder.writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
        clientBuilder.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)

        return clientBuilder.build()
    }

    @Provides
    @Singleton
    fun providesGson(): Gson {
        val collectionType: Type = object : TypeToken<PaginatedResultDto<TvShowDto>>() {}.type

        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            .registerTypeAdapter(collectionType, DeserializerPaginatedResultDto<TvShowDto>())
            .registerTypeAdapter(TvShowDetailDto::class.java, DeserializerTvShowDetailDto())
            .registerTypeAdapter(TvShowCreditsDto::class.java, DeserializerTvShowCreditsDto())
            .serializeNulls()
            .create()
    }


    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    @Provides
    open fun provideTvServiceService(retrofit: Retrofit): TvService = retrofit.create(TvService::class.java)
}

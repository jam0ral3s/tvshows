package com.jmorales.aba.base.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jmorales.aba.ui.showDetail.detail.TvShowDetailFragmentViewModel
import com.jmorales.aba.ui.showDetail.related.TvShowRelatedFragmentViewModel
import com.jmorales.aba.ui.showsList.TvShowsListActivityViewModel
import com.jmorales.aba.utils.extensions.ViewModelFactory
import com.jmorales.aba.utils.extensions.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(TvShowsListActivityViewModel::class)
    internal abstract fun tvShowsListActivityViewModel(viewModel: TvShowsListActivityViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(TvShowDetailFragmentViewModel::class)
    internal abstract fun tvShowDetailActivityViewModel(viewModel: TvShowDetailFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TvShowRelatedFragmentViewModel::class)
    internal abstract fun tvShowRelatedFragmentViewModel(viewModel: TvShowRelatedFragmentViewModel): ViewModel


}

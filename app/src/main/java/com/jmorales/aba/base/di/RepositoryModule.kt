package com.jmorales.aba.base.di

import com.jmorales.data.repository.TvShowsRepositoryImpl
import com.jmorales.domain.repository.TvShowsRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindTvShowsRepository(tvShowsRepository: TvShowsRepositoryImpl): TvShowsRepository


}

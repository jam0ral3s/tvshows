package com.jmorales.aba.ui.showDetail

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.jmorales.aba.R
import com.jmorales.aba.model.TvShowItem
import com.jmorales.aba.ui.showDetail.detail.TvShowDetailFragment
import com.jmorales.aba.ui.showDetail.related.TvShowRelatedFragment

private val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2
)

class TvShowDetailSectionsPagerAdapter(private val context: Context, fm: FragmentManager, private val tvShowItem: TvShowItem) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> TvShowDetailFragment.newInstance(tvShowItem)
            1 -> TvShowRelatedFragment.newInstance(tvShowItem)
            else -> throw Exception("Fragment error position")
        }

    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return 2
    }
}
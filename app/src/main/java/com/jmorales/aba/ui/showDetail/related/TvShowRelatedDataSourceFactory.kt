package com.jmorales.aba.ui.showDetail.related

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.jmorales.aba.model.TvShowItem
import com.jmorales.aba.model.TvShowRelatedItem
import com.jmorales.aba.model.TvShowRelatedItemMapper
import com.jmorales.domain.usecase.GetTvShowRelatedUseCase
import io.reactivex.disposables.CompositeDisposable

class TvShowRelatedDataSourceFactory(
    private val compositeDisposable: CompositeDisposable,
    private val tvShowItemMapper: TvShowRelatedItemMapper,
    private val id: Int,
    private val getTvShowRelatedUseCase: GetTvShowRelatedUseCase
) : DataSource.Factory<Long, TvShowRelatedItem>() {

    val usersDataSourceLiveData = MutableLiveData<TvShowRelatedDataSource>()

    override fun create(): DataSource<Long, TvShowRelatedItem> {
        val usersDataSource = TvShowRelatedDataSource(
            getTvShowRelatedUseCase,
            tvShowItemMapper,
            id,
            compositeDisposable
        )
        usersDataSourceLiveData.postValue(usersDataSource)
        return usersDataSource
    }
}
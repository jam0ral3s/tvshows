package com.jmorales.aba.ui.showDetail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.jmorales.aba.R
import com.jmorales.aba.model.TvShowItem

class TvShowDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tv_show_detail)

        val tvShowItem = intent?.extras?.getParcelable<TvShowItem>(TvShowItem.EXTRA_TVSHOWITEM)
        if (tvShowItem == null) {
            finish()
        } else {
            val sectionsPagerAdapter = TvShowDetailSectionsPagerAdapter(this, supportFragmentManager, tvShowItem)
            val viewPager: ViewPager = findViewById(R.id.view_pager)
            viewPager.adapter = sectionsPagerAdapter

            val tabs: TabLayout = findViewById(R.id.tabs)
            tabs.setupWithViewPager(viewPager)
        }
    }
}
package com.jmorales.aba.ui.showDetail.related

import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.jmorales.aba.model.TvShowItem
import com.jmorales.aba.model.TvShowRelatedItem


class TvShowsRelatedAdapter(private val clickListener: (TvShowRelatedItem) -> Unit) :
    PagedListAdapter<TvShowRelatedItem, RecyclerView.ViewHolder>(REPO_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TvShowRelatedViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val movieItem = getItem(position)
        if (movieItem != null) {
            (holder as TvShowRelatedViewHolder).bind(movieItem, clickListener)
        }
    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<TvShowRelatedItem>() {
            override fun areItemsTheSame(oldItem: TvShowRelatedItem, newItem: TvShowRelatedItem): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: TvShowRelatedItem, newItem: TvShowRelatedItem): Boolean =
                oldItem == newItem
        }
    }


}
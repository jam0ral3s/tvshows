package com.jmorales.aba.ui.showsList

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.jmorales.aba.model.TvShowItem
import com.jmorales.aba.model.TvShowItemMapper
import com.jmorales.domain.usecase.GetPopularTvShowsUseCase
import io.reactivex.disposables.CompositeDisposable

class TvShowsDataSourceFactory(
    private val compositeDisposable: CompositeDisposable,
    private val tvShowItemMapper: TvShowItemMapper,
    private val getPopularTvShowsUseCase: GetPopularTvShowsUseCase
) : DataSource.Factory<Long, TvShowItem>() {

    val usersDataSourceLiveData = MutableLiveData<TvShowsDataSource>()

    override fun create(): DataSource<Long, TvShowItem> {
        val usersDataSource = TvShowsDataSource(
            getPopularTvShowsUseCase,
            tvShowItemMapper,
            compositeDisposable
        )
        usersDataSourceLiveData.postValue(usersDataSource)
        return usersDataSource
    }
}
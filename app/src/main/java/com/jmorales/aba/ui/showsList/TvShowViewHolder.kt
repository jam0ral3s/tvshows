package com.jmorales.aba.ui.showsList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jmorales.aba.App
import com.jmorales.aba.R
import com.jmorales.aba.model.TvShowItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_tv_show.*


class TvShowViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    fun bind(movie: TvShowItem?, listener: (TvShowItem) -> Unit) {
        if (movie != null) {
            with(movie) {
                tvBg.contentDescription = name

                if (posterPath != null) {
                    //todo: ScaleType Matrix or customView ImageView
                    Glide.with(tvBg.context).load("https://image.tmdb.org/t/p/w300/$posterPath").centerCrop().into(tvBg)
                } else {
                    Glide.with(tvBg.context).load(R.drawable.no_image).fitCenter().into(tvBg)
                }

                tvTitle.text = name
                tvDesc.text = overview

                // Rating based on 5 instead of 10
                rating.rating = voteAverage.toFloat() / 2
                totalVotes.text = App.getAppContext().getString(R.string.totalVotes, voteCount)

                tvMore.setOnClickListener {
                    listener(this)
                }
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup): TvShowViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_tv_show, parent, false)
            return TvShowViewHolder(view)
        }
    }
}
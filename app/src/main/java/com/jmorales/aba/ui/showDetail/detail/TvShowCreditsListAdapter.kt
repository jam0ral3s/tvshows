package com.jmorales.aba.ui.showDetail.detail

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jmorales.aba.model.TvShowCreditItem


class TvShowCreditsListAdapter(private val creditsItems: List<TvShowCreditItem>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TvShoCreditsViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val creditItem = creditsItems[position]
        (holder as TvShoCreditsViewHolder).bind(creditItem)
    }

    override fun getItemCount(): Int {
        return creditsItems.size
    }
}
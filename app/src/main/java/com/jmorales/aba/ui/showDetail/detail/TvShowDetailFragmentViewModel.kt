package com.jmorales.aba.ui.showDetail.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jmorales.aba.model.*
import com.jmorales.aba.utils.extensions.DataException
import com.jmorales.aba.utils.extensions.DataState
import com.jmorales.aba.utils.extensions.addToCompositeDisposable
import com.jmorales.domain.usecase.GetTvShowCreditsUseCase
import com.jmorales.domain.usecase.GetTvShowUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class TvShowDetailFragmentViewModel @Inject constructor(
    val getTvShowUseCase: GetTvShowUseCase,
    val getTvShowCreditsUseCase: GetTvShowCreditsUseCase,
    val mapperTvShowDetail: TvShowDetailItemMapper,
    val mapperTvCreditItemMapper: TvShowCreditItemMapper

) : ViewModel() {
    private val compositeDisposable = CompositeDisposable()

    val tvShowItemData = MutableLiveData<TvShowItem>()
    val tvShowDetailItemData = MutableLiveData<DataException<TvShowDetailItem>>()
    val tvShowCreditsItemData = MutableLiveData<DataException<List<TvShowCreditItem>>>()


    var tvShowItem: TvShowItem? = null
        set(value) {
            if (field == null && value != null) {
                field = value
                tvShowItemData.postValue(field)
            }
        }

    fun getAdditionalInfo() {
        tvShowItem?.let {
            getTvShowUseCase.execute(it.id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleGetTvShowUseCaseResult(it) }
                .addToCompositeDisposable(compositeDisposable)
        }
    }

    private fun handleGetTvShowUseCaseResult(result: GetTvShowUseCase.CustomResult) {
        when (result) {
            is GetTvShowUseCase.CustomResult.Loading -> {
                tvShowDetailItemData.postValue(DataException(DataState.LOADING, null, null))
            }
            is GetTvShowUseCase.CustomResult.Success -> {
                tvShowDetailItemData.postValue(DataException(DataState.SUCCESS, mapperTvShowDetail.mapToPresentation(result.tvShowDetail), null))
            }
            is GetTvShowUseCase.CustomResult.Failure -> {
                tvShowDetailItemData.postValue(DataException(DataState.ERROR, tvShowDetailItemData.value?.data, result.throwable))
            }
        }
    }

    fun getCredits() {
        tvShowItem?.let {
            getTvShowCreditsUseCase.execute(it.id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleGetTvShowCreditsUseCase(it) }
                .addToCompositeDisposable(compositeDisposable)
        }
    }

    private fun handleGetTvShowCreditsUseCase(result: GetTvShowCreditsUseCase.CustomResult) {
        when (result) {
            is GetTvShowCreditsUseCase.CustomResult.Loading -> {
                tvShowCreditsItemData.postValue(DataException(DataState.LOADING, null, null))
            }
            is GetTvShowCreditsUseCase.CustomResult.Success -> {
                tvShowCreditsItemData.postValue(DataException(DataState.SUCCESS, mapperTvCreditItemMapper.mapToPresentation(result.tvShowCredits), null))
            }
            is GetTvShowCreditsUseCase.CustomResult.Failure -> {
                tvShowCreditsItemData.postValue(DataException(DataState.ERROR, tvShowCreditsItemData.value?.data, result.throwable))
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}
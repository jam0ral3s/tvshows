package com.jmorales.aba.ui.showsList

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.jmorales.aba.model.TvShowItem
import com.jmorales.aba.model.TvShowItemMapper
import com.jmorales.aba.utils.PaginationStatus
import com.jmorales.aba.utils.extensions.addToCompositeDisposable
import com.jmorales.domain.usecase.GetPopularTvShowsUseCase
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers

class TvShowsDataSource(
    private val getPopularTvShowsUseCase: GetPopularTvShowsUseCase,
    private val tvShowItemMapper: TvShowItemMapper,
    private val compositeDisposable: CompositeDisposable
) : PageKeyedDataSource<Long, TvShowItem>() {

    private var retryCompletable: Completable? = null

    val networkState = MutableLiveData<PaginationStatus>()

    override fun loadInitial(params: LoadInitialParams<Long>, callback: LoadInitialCallback<Long, TvShowItem>) {
        val currentPage = 1
        val nextPage = currentPage + 1

        getPopularTvShowsUseCase.execute(currentPage)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it) {
                    is GetPopularTvShowsUseCase.CustomResult.Loading -> {
                    }
                    is GetPopularTvShowsUseCase.CustomResult.Success -> {
                        setRetry(null)
                        networkState.postValue(PaginationStatus.LOADED)


                        val items = tvShowItemMapper.mapToPresentation(it.paginatedTvShows.results)
                        callback.onResult(
                            items,
                            currentPage,
                            it.paginatedTvShows.totalResult,
                            currentPage.toLong(),
                            nextPage.toLong()
                        )
                    }
                    is GetPopularTvShowsUseCase.CustomResult.Failure -> {
                        setRetry(Action { loadInitial(params, callback) })

                        val error = PaginationStatus.error(it.throwable.message)
                        networkState.postValue(error)
                    }
                }
            }, {
                setRetry(Action { loadInitial(params, callback) })
            }).addToCompositeDisposable(compositeDisposable)
    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, TvShowItem>) {

        val currentPage = params.key
        val nextPage = currentPage + 1

        getPopularTvShowsUseCase.execute(params.key.toInt())
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it) {
                    is GetPopularTvShowsUseCase.CustomResult.Loading -> {
                    }
                    is GetPopularTvShowsUseCase.CustomResult.Success -> {
                        setRetry(null)
                        networkState.postValue(PaginationStatus.LOADED)

                        val items = tvShowItemMapper.mapToPresentation(it.paginatedTvShows.results)
                        callback.onResult(items, nextPage)
                    }
                    is GetPopularTvShowsUseCase.CustomResult.Failure -> {
                        setRetry(Action { loadAfter(params, callback) })
                        val error = PaginationStatus.error(it.throwable.message)
                        networkState.postValue(error)
                    }
                }
            }, {
                setRetry(Action { loadAfter(params, callback) })
            }).addToCompositeDisposable(compositeDisposable)
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, TvShowItem>) {
    }


    fun retry() {
        if (retryCompletable != null) {
            retryCompletable!!
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ }, { })
                .addToCompositeDisposable(compositeDisposable)
        }
    }

    private fun setRetry(action: Action?) {
        if (action == null) {
            this.retryCompletable = null
        } else {
            this.retryCompletable = Completable.fromAction(action)
        }
    }


}
package com.jmorales.aba.ui.splash

import android.animation.Animator
import android.content.Intent
import com.jmorales.aba.R
import com.jmorales.aba.base.BaseActivity
import com.jmorales.aba.ui.showsList.TvShowsListActivity
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : BaseActivity() {
    override fun getActivityLayout(): Int = R.layout.activity_splash

    override fun onStart() {
        super.onStart()
        noSignal.animate().scaleY(0f).setDuration(100).setStartDelay(1000)
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {
                }

                override fun onAnimationEnd(animation: Animator?) {
                    startActivity(Intent(baseContext, TvShowsListActivity::class.java))
                    overridePendingTransition(R.anim.enter_animation, R.anim.exit_animation)
                    finish()
                }

                override fun onAnimationCancel(animation: Animator?) {
                }

                override fun onAnimationStart(animation: Animator?) {
                }

            })
    }
}
package com.jmorales.aba.ui.showDetail.related

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.jmorales.aba.model.TvShowRelatedItem
import com.jmorales.aba.model.TvShowRelatedItemMapper
import com.jmorales.aba.utils.PaginationStatus
import com.jmorales.aba.utils.extensions.addToCompositeDisposable
import com.jmorales.domain.usecase.GetTvShowRelatedUseCase
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers

class TvShowRelatedDataSource(
    private val getTvShowRelatedUseCase: GetTvShowRelatedUseCase,
    private val tvShowRelatedItemMapper: TvShowRelatedItemMapper,
    private val id: Int,
    private val compositeDisposable: CompositeDisposable
) : PageKeyedDataSource<Long, TvShowRelatedItem>() {

    private var retryCompletable: Completable? = null

    val networkState = MutableLiveData<PaginationStatus>()

    override fun loadInitial(params: LoadInitialParams<Long>, callback: LoadInitialCallback<Long, TvShowRelatedItem>) {
        val currentPage = 1
        val nextPage = currentPage + 1

        getTvShowRelatedUseCase.execute(id, currentPage)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it) {
                    is GetTvShowRelatedUseCase.CustomResult.Loading -> {
                    }
                    is GetTvShowRelatedUseCase.CustomResult.Success -> {
                        setRetry(null)
                        networkState.postValue(PaginationStatus.LOADED)


                        val items = tvShowRelatedItemMapper.mapToPresentation(it.paginatedTvShows.results)
                        callback.onResult(
                            items,
                            currentPage,
                            it.paginatedTvShows.totalResult,
                            currentPage.toLong(),
                            nextPage.toLong()
                        )
                    }
                    is GetTvShowRelatedUseCase.CustomResult.Failure -> {
                        setRetry(Action { loadInitial(params, callback) })

                        val error = PaginationStatus.error(it.throwable.message)
                        networkState.postValue(error)
                    }
                }
            }, {
                setRetry(Action { loadInitial(params, callback) })
            }).addToCompositeDisposable(compositeDisposable)

    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, TvShowRelatedItem>) {

        val currentPage = params.key
        val nextPage = currentPage + 1

        getTvShowRelatedUseCase.execute(id, params.key.toInt())
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it) {
                    is GetTvShowRelatedUseCase.CustomResult.Loading -> {
                    }
                    is GetTvShowRelatedUseCase.CustomResult.Success -> {
                        setRetry(null)
                        networkState.postValue(PaginationStatus.LOADED)

                        val items = tvShowRelatedItemMapper.mapToPresentation(it.paginatedTvShows.results)
                        callback.onResult(items, nextPage)
                    }
                    is GetTvShowRelatedUseCase.CustomResult.Failure -> {
                        setRetry(Action { loadAfter(params, callback) })
                        val error = PaginationStatus.error(it.throwable.message)
                        networkState.postValue(error)
                    }
                }
            }, {
                setRetry(Action { loadAfter(params, callback) })
            }
            ).addToCompositeDisposable(compositeDisposable)
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, TvShowRelatedItem>) {
    }


    fun retry() {
        if (retryCompletable != null) {
            retryCompletable!!
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ }, { })
                .addToCompositeDisposable(compositeDisposable)
        }
    }

    private fun setRetry(action: Action?) {
        if (action == null) {
            this.retryCompletable = null
        } else {
            this.retryCompletable = Completable.fromAction(action)
        }
    }


}
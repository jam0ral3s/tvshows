package com.jmorales.aba.ui.showDetail.related

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.jmorales.aba.model.TvShowItem
import com.jmorales.aba.model.TvShowItemMapper
import com.jmorales.aba.model.TvShowRelatedItem
import com.jmorales.aba.model.TvShowRelatedItemMapper
import com.jmorales.aba.utils.PaginationStatus
import com.jmorales.domain.usecase.GetPopularTvShowsUseCase
import com.jmorales.domain.usecase.GetTvShowRelatedUseCase
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class TvShowRelatedFragmentViewModel @Inject constructor(
    private val getTvShowRelatedUseCase: GetTvShowRelatedUseCase,
    private val tvShowItemMapper: TvShowRelatedItemMapper
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    var tvShowItem: TvShowItem? = null
        set(value) {
            if (field == null && value != null) {
                field = value
                tvShowItemData.postValue(field)
            }
        }

    val tvShowItemData = MutableLiveData<TvShowItem>()
    lateinit var tvShowsRelated: LiveData<PagedList<TvShowRelatedItem>>
    lateinit var sourceFactory: TvShowRelatedDataSourceFactory


    fun initializeRelatedList() {
        sourceFactory = TvShowRelatedDataSourceFactory(
            compositeDisposable,
            tvShowItemMapper,
            tvShowItem!!.id,
            getTvShowRelatedUseCase
        )
        val config = PagedList.Config.Builder()
            .setPageSize(20)
            .setInitialLoadSizeHint(20 * 2)
            .setEnablePlaceholders(false)
            .build()

        tvShowsRelated = LivePagedListBuilder<Long, TvShowRelatedItem>(sourceFactory, config).build()
    }

    fun retry() {
        sourceFactory.usersDataSourceLiveData.value!!.retry()
    }

    fun refresh() {
        sourceFactory.usersDataSourceLiveData.value!!.invalidate()
    }

    fun getNetworkState(): LiveData<PaginationStatus> = Transformations.switchMap<TvShowRelatedDataSource, PaginationStatus>(
        sourceFactory.usersDataSourceLiveData
    ) { it.networkState }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}
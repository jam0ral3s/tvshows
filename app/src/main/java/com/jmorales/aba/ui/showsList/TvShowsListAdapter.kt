package com.jmorales.aba.ui.showsList

import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.jmorales.aba.model.TvShowItem


class TvShowsListAdapter(private val clickListener: (TvShowItem) -> Unit) :
    PagedListAdapter<TvShowItem, RecyclerView.ViewHolder>(REPO_COMPARATOR) {

    // Used for show fadeAnimation only one time
    private var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TvShowViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val movieItem = getItem(position)
        if (movieItem != null) {
            (holder as TvShowViewHolder).bind(movieItem, clickListener)
            setFadeAnimation(holder.itemView, position)
        }
    }

    private fun setFadeAnimation(view: View, position: Int) {
        if (position > lastPosition) {
            val anim = AlphaAnimation(0.0f, 1.0f)
            anim.duration = 200
            view.startAnimation(anim)
            lastPosition = position
        }
    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<TvShowItem>() {
            override fun areItemsTheSame(oldItem: TvShowItem, newItem: TvShowItem): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: TvShowItem, newItem: TvShowItem): Boolean =
                oldItem == newItem
        }
    }


}
package com.jmorales.aba.ui.showsList

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagedList
import com.jmorales.aba.R
import com.jmorales.aba.base.BaseActivity
import com.jmorales.aba.model.TvShowItem
import com.jmorales.aba.ui.showDetail.TvShowDetailActivity
import com.jmorales.aba.utils.PaginationStatus
import com.jmorales.aba.utils.SpacesItemDecoration
import com.jmorales.aba.utils.extensions.getAppInjector
import com.jmorales.aba.utils.extensions.observe
import com.jmorales.aba.utils.extensions.withViewModel
import kotlinx.android.synthetic.main.activity_tv_shows_list.*
import javax.inject.Inject

class TvShowsListActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: TvShowsListActivityViewModel

    private lateinit var tvShowsListAdapter: TvShowsListAdapter

    override fun getActivityLayout(): Int = R.layout.activity_tv_shows_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getAppInjector().inject(this)

        title = getString(R.string.tvShows)

        withViewModel<TvShowsListActivityViewModel>(viewModelFactory) {
            viewModel = this

            observe(tvShows, ::onTvShowsChange)
            observe(getNetworkState(), ::onPaginationStatus)
        }

        configureTvShowsList()

        retry.setOnClickListener {
            viewModel.retry()
        }

    }

    private fun configureTvShowsList() {
        tvShowsListAdapter = TvShowsListAdapter {
            val intent = Intent(baseContext, TvShowDetailActivity::class.java).apply {
                putExtra(TvShowItem.EXTRA_TVSHOWITEM, it)
            }
            startActivity(intent)
        }

        tvShowsList.apply {
            val spacingInPixels = resources.getDimensionPixelSize(R.dimen.margin_recycler)
            addItemDecoration(SpacesItemDecoration(spacingInPixels))

            setHasFixedSize(true)
            adapter = tvShowsListAdapter
        }

        swipeRefreshLayout.setOnRefreshListener {
            viewModel.refresh()
        }
    }

    private fun onPaginationStatus(paginationStatus: PaginationStatus?) {
        paginationStatus?.let {
            when (it) {
                PaginationStatus.LOADED -> {
                    swipeRefreshLayout.isRefreshing = false
                    retry.hide()
                    tvShowsListAdapter.notifyDataSetChanged()
                }
                else -> {
                    retry.show()
                }
            }
        }
    }

    private fun onTvShowsChange(pagedList: PagedList<TvShowItem>?) {
        tvShowsListAdapter.submitList(pagedList)
    }

}

package com.jmorales.aba.ui.showDetail.related

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jmorales.aba.R
import com.jmorales.aba.model.TvShowItem
import com.jmorales.aba.model.TvShowRelatedItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_tv_show_related.*


class TvShowRelatedViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    fun bind(tvShowRelated: TvShowRelatedItem?, listener: (TvShowRelatedItem) -> Unit) {
        if (tvShowRelated != null) {
            with(tvShowRelated) {
                tvShowPoster.contentDescription = name

                if (posterPath != null) {
                    Glide.with(tvShowPoster.context).load("https://image.tmdb.org/t/p/w300/$posterPath").centerCrop().into(tvShowPoster)
                } else {
                    Glide.with(tvShowPoster.context).load(R.drawable.no_image).fitCenter().into(tvShowPoster)
                }

                tvShowTitle.text = name

                relatedContainer.setOnClickListener {
                    listener(tvShowRelated)
                }
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup): TvShowRelatedViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_tv_show_related, parent, false)
            return TvShowRelatedViewHolder(view)
        }
    }
}
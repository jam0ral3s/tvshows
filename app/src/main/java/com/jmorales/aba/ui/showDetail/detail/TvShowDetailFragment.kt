package com.jmorales.aba.ui.showDetail.detail

import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.core.view.ViewCompat
import androidx.lifecycle.ViewModelProvider
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.chip.Chip
import com.jmorales.aba.R
import com.jmorales.aba.base.BaseFragment
import com.jmorales.aba.model.TvShowCreditItem
import com.jmorales.aba.model.TvShowDetailItem
import com.jmorales.aba.model.TvShowItem
import com.jmorales.aba.utils.GridSpacesItemDecoration
import com.jmorales.aba.utils.SpacesItemDecoration
import com.jmorales.aba.utils.extensions.*
import kotlinx.android.synthetic.main.fragment_tv_show_detail.*
import javax.inject.Inject


class TvShowDetailFragment : BaseFragment() {
    override fun getFragmentLayout() = R.layout.fragment_tv_show_detail

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: TvShowDetailFragmentViewModel

    private lateinit var creditsListAdapter: TvShowCreditsListAdapter

    var textColor: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getAppInjector().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        withViewModel<TvShowDetailFragmentViewModel>(viewModelFactory) {
            viewModel = this

            tvShowItem = arguments?.getParcelable<TvShowItem>(TvShowItem.EXTRA_TVSHOWITEM)

            observe(tvShowItemData, ::onTvShowItem)
            observe(tvShowDetailItemData, ::onTvShowDetailUpdate)
            observe(tvShowCreditsItemData, ::onTvShowCreditsUpdate)
        }
    }

    private fun onTvShowItem(tvShowItem: TvShowItem?) {
        tvShowItem?.let {
            configureBackground(tvShowItem)

            if (tvShowItem.posterPath != null) {
                Glide.with(header.context).load("https://image.tmdb.org/t/p/w300/${tvShowItem.posterPath}").centerCrop()
                    .into(header)
            } else {
                Glide.with(header.context).load(R.drawable.no_image).fitCenter().into(header)
            }

            headerTitle.text = tvShowItem.name
            overview.text = tvShowItem.overview
        }

        viewModel.getAdditionalInfo()
        viewModel.getCredits()

    }

    private fun onTvShowDetailUpdate(data: DataException<TvShowDetailItem>?) {
        data?.let {
            when (it.dataState) {
                DataState.LOADING -> {
                }
                DataState.SUCCESS -> {
                    onTvShowDetailItem(it.data)
                }
                DataState.ERROR -> {
                    Toast.makeText(
                        app.applicationContext,
                        getString(R.string.error_retrieving_data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun onTvShowCreditsUpdate(data: DataException<List<TvShowCreditItem>>?) {
        data?.let {
            when (it.dataState) {
                DataState.LOADING -> {
                }
                DataState.SUCCESS -> {
                    initializeAdapter(it.data)
                }
                DataState.ERROR -> {
                    Toast.makeText(
                        app.applicationContext,
                        getString(R.string.error_retrieving_data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun initializeAdapter(credits: List<TvShowCreditItem>?) {
        credits?.let {
            val spacingInPixels = resources.getDimensionPixelSize(R.dimen.margin_grid)

            creditsListAdapter = TvShowCreditsListAdapter(it)
            creditsRv.adapter = creditsListAdapter
            creditsRv.addItemDecoration(GridSpacesItemDecoration(spacingInPixels))

            castContainer.visibility = View.VISIBLE
        }

    }

    private fun onTvShowDetailItem(tvShowDetailItem: TvShowDetailItem?) {
        tvShowDetailItem?.let {
            createGenreChips(it.genres)
            seasons.text = getString(R.string.number_seasons, it.numberSeasons)
        }
    }

    private fun createGenreChips(genresList: List<String>) {

        genresList.map {
            val chip = Chip(activity)
            chip.text = it
            chip.isClickable = false
            chip.isCheckable = false
            chip.isCloseIconVisible = false
            chip.setTextColor(Color.BLACK)

            val csl = ColorStateList.valueOf(Color.WHITE)
            chip.chipBackgroundColor = csl

            genres.addView(chip)

        }

    }


    /**
     * Get dominant color of image and create overlay to put in front of image
     */
    private fun configureBackground(tvShowItem: TvShowItem) {
        if (tvShowItem.posterPath != null) {
            Glide.with(this).asBitmap().load("https://image.tmdb.org/t/p/w500/${tvShowItem.posterPath}")
                .into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {

                        Palette.Builder(resource).generate {
                            it?.let { palette ->
                                val dominantColor = palette.getDominantColor(
                                    ContextCompat.getColor(
                                        app.applicationContext,
                                        R.color.black
                                    )
                                )
                                val alphaColor = ColorUtils.setAlphaComponent(dominantColor, 220)
                                bgOverlay.setBackgroundColor(alphaColor)


                                val red = Color.red(dominantColor)
                                val blue = Color.blue(dominantColor)
                                val green = Color.green(dominantColor)

                                textColor = getContrastColor(red, blue, green)

                                headerTitle.setTextColor(textColor ?: Color.WHITE)
                                overview.setTextColor(textColor ?: Color.WHITE)
                                seasons.setTextColor(textColor ?: Color.WHITE)
                                castTitle.setTextColor(textColor ?: Color.WHITE)
                            }
                        }

                        bg.setImageBitmap(resource)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                    }
                })
        }
    }

    fun getContrastColor(red: Int, green: Int, blue: Int): Int {
        val tmpRed = if (red == 0) 1 else red
        val tmpGreen = if (green == 0) 1 else green
        val tmpBlue = if (blue == 0) 1 else blue

        val y = (299 * tmpRed + 587 * tmpGreen + 114 * tmpBlue) / 1000
        return if (y >= 160) Color.BLACK else Color.WHITE
    }

    companion object {
        @JvmStatic
        fun newInstance(tvShowItem: TvShowItem): TvShowDetailFragment {
            return TvShowDetailFragment().apply {
                arguments = Bundle().apply { putParcelable(TvShowItem.EXTRA_TVSHOWITEM, tvShowItem) }
            }
        }
    }
}

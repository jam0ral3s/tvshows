package com.jmorales.aba.ui.showsList

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.jmorales.aba.model.TvShowItem
import com.jmorales.aba.model.TvShowItemMapper
import com.jmorales.aba.utils.PaginationStatus
import com.jmorales.domain.usecase.GetPopularTvShowsUseCase
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class TvShowsListActivityViewModel @Inject constructor(
    getPopularTvShowsUseCase: GetPopularTvShowsUseCase,
    tvShowItemMapper: TvShowItemMapper
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val tvShows: LiveData<PagedList<TvShowItem>>
    val sourceFactory: TvShowsDataSourceFactory

    init {
        sourceFactory = TvShowsDataSourceFactory(
            compositeDisposable,
            tvShowItemMapper,
            getPopularTvShowsUseCase
        )
        val config = PagedList.Config.Builder()
            .setPageSize(20)
            .setInitialLoadSizeHint(20 * 2)
            .setEnablePlaceholders(false)
            .build()
        tvShows = LivePagedListBuilder<Long, TvShowItem>(sourceFactory, config).build()
    }

    fun retry() {
        sourceFactory.usersDataSourceLiveData.value!!.retry()
    }

    fun refresh() {
        sourceFactory.usersDataSourceLiveData.value!!.invalidate()
    }

    fun getNetworkState(): LiveData<PaginationStatus> = Transformations.switchMap<TvShowsDataSource, PaginationStatus>(
        sourceFactory.usersDataSourceLiveData
    ) { it.networkState }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}
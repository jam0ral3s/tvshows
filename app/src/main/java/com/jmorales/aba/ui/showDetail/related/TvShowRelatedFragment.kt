package com.jmorales.aba.ui.showDetail.related

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagedList
import com.jmorales.aba.R
import com.jmorales.aba.base.BaseFragment
import com.jmorales.aba.model.TvShowItem
import com.jmorales.aba.model.TvShowRelatedItem
import com.jmorales.aba.ui.showDetail.TvShowDetailActivity
import com.jmorales.aba.utils.GridSpacesItemDecoration
import com.jmorales.aba.utils.PaginationStatus
import com.jmorales.aba.utils.SpacesItemDecoration
import com.jmorales.aba.utils.extensions.getAppInjector
import com.jmorales.aba.utils.extensions.observe
import com.jmorales.aba.utils.extensions.withViewModel
import kotlinx.android.synthetic.main.fragment_tv_shows_related.*
import javax.inject.Inject

class TvShowRelatedFragment : BaseFragment() {
    override fun getFragmentLayout(): Int = R.layout.fragment_tv_shows_related

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: TvShowRelatedFragmentViewModel

    private lateinit var tvShowsRelatedAdapter: TvShowsRelatedAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getAppInjector().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureTvShowsList()

        withViewModel<TvShowRelatedFragmentViewModel>(viewModelFactory) {
            viewModel = this
            tvShowItem = arguments?.getParcelable<TvShowItem>(TvShowItem.EXTRA_TVSHOWITEM)

            observe(tvShowItemData, ::onTvShowItem)

            retryRelated?.setOnClickListener {
                viewModel.retry()
            }
        }
    }

    private fun onTvShowItem(tvShowItem: TvShowItem?) {
        viewModel.initializeRelatedList()

        withViewModel<TvShowRelatedFragmentViewModel>(viewModelFactory) {
            observe(tvShowsRelated, ::onTvShowsRelatedChange)
            observe(getNetworkState(), ::onPaginationStatus)
        }
    }

    private fun configureTvShowsList() {
        tvShowsRelatedAdapter = TvShowsRelatedAdapter {

            val intent = Intent(activity?.baseContext, TvShowDetailActivity::class.java).apply {
                putExtra(
                    TvShowItem.EXTRA_TVSHOWITEM,
                    TvShowItem(it.id, it.name, it.posterPath, 0, it.overview, 0)
                )
            }
            startActivity(intent)
            activity?.finish()
        }

        tvShowsList.apply {
            val spacingInPixels = resources.getDimensionPixelSize(R.dimen.margin_grid)
            addItemDecoration(GridSpacesItemDecoration(spacingInPixels))

            setHasFixedSize(true)
            adapter = tvShowsRelatedAdapter
        }

        swipeRefreshLayout.setOnRefreshListener {
            viewModel.refresh()
        }
    }

    private fun onPaginationStatus(paginationStatus: PaginationStatus?) {
        paginationStatus?.let {
            when (it) {
                PaginationStatus.LOADED -> {
                    tvShowsRelatedAdapter.notifyDataSetChanged()
                    swipeRefreshLayout.isRefreshing = false
                    retryRelated.hide()
                }
                else -> {
                    retryRelated.show()
                }
            }
        }
    }

    private fun onTvShowsRelatedChange(pagedList: PagedList<TvShowRelatedItem>?) {
        tvShowsRelatedAdapter.submitList(pagedList)
    }

    companion object {
        @JvmStatic
        fun newInstance(tvShowItem: TvShowItem): TvShowRelatedFragment {
            return TvShowRelatedFragment().apply {
                arguments = Bundle().apply { putParcelable(TvShowItem.EXTRA_TVSHOWITEM, tvShowItem) }
            }
        }
    }
}
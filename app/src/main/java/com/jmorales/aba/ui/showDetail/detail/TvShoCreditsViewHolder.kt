package com.jmorales.aba.ui.showDetail.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jmorales.aba.R
import com.jmorales.aba.model.TvShowCreditItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_tv_show_credit.*


class TvShoCreditsViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {
    fun bind(creditItem: TvShowCreditItem) {
        with(creditItem) {
            creditProfile.contentDescription = name

            if (profileImage != null) {
                Glide.with(creditProfile.context).load("https://image.tmdb.org/t/p/w300/$profileImage").centerCrop()
                    .into(creditProfile)
            } else {
                Glide.with(creditProfile.context).load(R.drawable.no_image).fitCenter().into(creditProfile)
            }

            creditName.text = name
            creditCharacter.text = character

        }
    }

    companion object {
        fun create(parent: ViewGroup): TvShoCreditsViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_tv_show_credit, parent, false)
            return TvShoCreditsViewHolder(view)
        }
    }
}
package com.jmorales.aba

import com.jmorales.aba.base.di.ApiModule
import com.jmorales.data.services.TvService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior


@Module
class ApiModuleMock : ApiModule() {
    
    private val behavior = NetworkBehavior.create()

    @Provides
    override fun provideTvServiceService(retrofit: Retrofit): TvService {
        val mockRetrofit = MockRetrofit.Builder(retrofit).networkBehavior(behavior).build()
        val delegate = mockRetrofit.create(TvService::class.java)
        return TvServiceMock(delegate)
    }
}

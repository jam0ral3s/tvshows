package com.jmorales.aba.ui.showsList

import android.os.SystemClock.sleep
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnitRunner
import com.jmorales.aba.ApiModuleMock
import com.jmorales.aba.App
import com.jmorales.aba.R
import com.jmorales.aba.RecyclerViewMatcher
import com.jmorales.aba.base.di.DaggerInjector
import com.squareup.rx2.idler.Rx2Idler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class TvShowListsActivityEspressoTest: AndroidJUnitRunner() {

    @get:Rule
    val activityRule = ActivityTestRule<TvShowsListActivity>(TvShowsListActivity::class.java, false, false)


    @Before
    fun setup() {

        val myApp = InstrumentationRegistry
            .getInstrumentation()
            .targetContext
            .applicationContext as App

        val injector = DaggerInjector.builder()
            .apiModule(ApiModuleMock())
            .build()

        myApp.setDaggerInjector(injector)

        RxJavaPlugins.setInitNewThreadSchedulerHandler(Rx2Idler.create("Rxjava new thread Scheduler"))
        RxJavaPlugins.setInitIoSchedulerHandler(Rx2Idler.create("Rxjava IO Scheduler"))
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(Rx2Idler.create("RxAndroid MainThreadS Scheduler"))

        activityRule.launchActivity(null)
    }

    @Test
    @Throws(Exception::class)
    fun launchAndSearch() {

        onView(withId(R.id.tvShowsList)).check(matches(isDisplayed()));

        //todo: See how to remove this sleep with  better use of Rx2Idler
        sleep(500)

        onView(
            RecyclerViewMatcher(R.id.tvShowsList)
                .atPositionOnView(0, R.id.tvTitle)
        )
            .check(matches(ViewMatchers.withText("The Flash")))
    }
}

package com.jmorales.aba

import androidx.annotation.RawRes
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.jmorales.data.model.*
import com.jmorales.data.services.TvService
import io.reactivex.Observable
import retrofit2.mock.BehaviorDelegate
import java.io.InputStream
import java.lang.reflect.Type
import java.util.*


class TvServiceMock(private val delegate: BehaviorDelegate<TvService>) : TvService {


    override fun getPopularShows(options: Map<String, String>): Observable<PaginatedResultDto<TvShowDto>> {
        val jsonResponse = readRawResource(R.raw.popular_tv_shows)

        val typeToken = TypeToken.getParameterized(PaginatedResultDto::class.java, TvShowDto::class.java).type
        val response = providesGson().fromJson<PaginatedResultDto<TvShowDto>>(jsonResponse, typeToken)

        return delegate.returningResponse(response).getPopularShows(options)
    }

    override fun getDetailTvShow(id: Int): Observable<TvShowDetailDto> {
        return delegate.returningResponse("").getDetailTvShow(id)
    }

    override fun geTvShowCredits(id: Int): Observable<TvShowCreditsDto> {
        return delegate.returningResponse("").geTvShowCredits(id)
    }

    override fun geTvShowRelated(id: Int, options: Map<String, String>): Observable<PaginatedResultDto<TvShowDto>> {
        return delegate.returningResponse("").geTvShowRelated(id, options)
    }


    fun providesGson(): Gson {
        val collectionType: Type = object : TypeToken<PaginatedResultDto<TvShowDto>>() {}.type

        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            .registerTypeAdapter(collectionType, DeserializerPaginatedResultDto<TvShowDto>())
            .registerTypeAdapter(TvShowDetailDto::class.java, DeserializerTvShowDetailDto())
            .registerTypeAdapter(TvShowCreditsDto::class.java, DeserializerTvShowCreditsDto())
            .serializeNulls()
            .create()
    }

    fun readRawResource(@RawRes res: Int): String {
        return readStream(App.getAppContext().resources.openRawResource(res))
    }

    private fun readStream(inputSteam: InputStream): String {
        val s = Scanner(inputSteam).useDelimiter("\\A")
        return if (s.hasNext()) s.next() else ""
    }

}
